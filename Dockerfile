FROM alpine as test

CMD echo 'Built with kaniko from https://gitlab.com/guided-explorations/gitlab-ci-yml-tips-tricks-and-hacks/kaniko-docker-build/'

#When Kaniko Caching is being used each layer is immediately pushed before processing the next RUN command
RUN apk add git

RUN apk add openssh

FROM scratch

COPY --from=test / /

CMD [ "sh" ]
